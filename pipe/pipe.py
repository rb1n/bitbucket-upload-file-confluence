import os
import json
import glob
from pathlib import Path

import yaml
import requests
from requests.auth import HTTPBasicAuth

from bitbucket_pipes_toolkit import Pipe, get_logger


logger = get_logger()

schema = {
    'TITLE': {'type': 'string', 'required': True},
    'FILENAME': {'type': 'string', 'required': True},
    'TOKEN': {'type': 'string', 'required': True},
    'SPACE': {'type': 'string', 'required': True},
    'CONTENT_ID': {'type': 'number', 'required': True},
    'CONTENT_TYPE': {'type': 'string', 'required': True, 'default': 'page'},
    'DOMAIN': {'type': 'string', 'required': True},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class UploadFilePipe(Pipe):

    def run(self):
        super().run()

        logger.info('Executing the pipe...')

        title = self.get_variable('TITLE')
        token = self.get_variable('TOKEN')
        space = self.get_variable('SPACE')
        contentId = self.get_variable('CONTENT_ID')
        contentType = self.get_variable('CONTENT_TYPE')
        domain = self.get_variable('DOMAIN')
        token = self.get_variable('TOKEN')
        filename = self.get_variable('FILENAME')

        # Request Headers
        headers = {
            "Content-Type": "application/json",
            "Authorization": "Basic " + token
        }

        url = 'https://' + domain + '.atlassian.net/wiki/rest/api/content/' + str(contentId)

        r = requests.get(url, headers=headers)          

        logger.info('Status code is: ' + str(r.status_code))
        if r.status_code == 200:
            version = r.json()['version']['number']
            logger.info('Current version is: ' + str(version))

            with open(filename, 'r') as file:
                file = file.read()

                # Request body
                data = {
                    'id': str(contentId),
                    'type': contentType,
                    'title': title,
                    'space': {'key':space},
                    'body': {
                        'storage':{
                            'value': file,
                            'representation':'wiki',
                        }
                    },
                    'version': {
                        'number': version + 1,
                        'when': '2020-10-06T15:16:17.501-04:00'
                    }
                }

                logger.info('changelog data: ' + json.dumps(data));
                logger.info('changelog headers: ' + json.dumps(headers));
                logger.info('changelog url: ' + url);
                response = requests.put(url, headers=headers, data=json.dumps(data))       

                if response.status_code == 200:
                    logger.info('Updated post: ' + str(response.status_code))
                    logger.info('  New version is: ' + str(response.json()['version']['number']))
                else:
                    self.fail(str(response.status_code) + ": " + response.text)
        else:
            self.fail(str(response.status_code) + ": " + response.text)


        



if __name__ == '__main__':
    metadata = yaml.safe_load(open('/pipe.yml', 'r'))
    pipe = UploadFilePipe(schema=schema, pipe_metadata=metadata, check_for_newer_version=True)
    pipe.run()
