# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.0.15

- patch: use wiki

## 1.0.14

- patch: jsondumps

## 1.0.13

- patch: meh

## 1.0.12

- patch: log and fake

## 1.0.11

- patch: put

## 1.0.10

- patch: use dict properly

## 1.0.9

- patch: stringify
- patch: stringify 2

## 1.0.8

- patch: accept number

## 1.0.7

- patch: accept number

## 1.0.6

- patch: fix py

## 1.0.5

- patch: fix py

## 1.0.4

- patch: fix ==

## 1.0.3

- patch: use latest

## 1.0.2

- patch: refactor

## 1.0.1

- patch: fix read file

## 1.0.0

- major: initial

